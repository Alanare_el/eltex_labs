package com.example.demo.leha.lab7.Exception;

public class OrderDelException extends Exception {

    private int code;

    public OrderDelException(int code){
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "OrderDelException{" +
                "code=" + code +
                '}';
    }
}

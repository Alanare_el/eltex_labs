package com.example.demo.leha.lab7.dao;

import com.example.demo.leha.lab2.Order;
import com.example.demo.leha.lab5.AManageOrder;
import com.example.demo.leha.lab5.ManagerOrderJSON;
import com.example.demo.leha.lab6.Helper;
import com.example.demo.leha.lab7.Exception.OrderDelException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Repository;

import java.util.PriorityQueue;

@Repository
public class OrderDao {

    private Gson gson = new GsonBuilder().create();
    private PriorityQueue<Order> orders = new PriorityQueue<>();

    private AManageOrder managerOrder = new ManagerOrderJSON(orders);

    public OrderDao() throws OrderDelException {
    }

    public String getAllOrders() throws OrderDelException {
        return gson.toJson(managerOrder.readAll());
    }

    public String getById(String id) {
        return gson.toJson(managerOrder.readById(id));
    }

    public String addToCard(String id) throws OrderDelException{
        return managerOrder.addToCard(id);
    }

    public String delById(String id) throws OrderDelException {
        return managerOrder.delById(id);
    }
}

package com.example.demo.leha.lab7.service;

import com.example.demo.leha.lab7.Exception.OrderDelException;
import com.example.demo.leha.lab7.dao.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class OrderService {
    @Autowired
    private OrderDao orderDao;

    public String getAllOrders() throws OrderDelException {
        return orderDao.getAllOrders();
    }

    public String getById(String id) {
        return orderDao.getById(id);
    }

    public String addToCard(String id) throws OrderDelException{
        return orderDao.addToCard(id);
    }

    public String delById(String id) throws OrderDelException {
        return orderDao.delById(id);
    }
}

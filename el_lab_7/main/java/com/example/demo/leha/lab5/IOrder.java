package com.example.demo.leha.lab5;

import com.example.demo.leha.lab2.Order;
import com.example.demo.leha.lab7.Exception.OrderDelException;

import java.util.PriorityQueue;

public interface IOrder {

    Order readById(String id);

    void saveById(String id);

    PriorityQueue readAll() throws OrderDelException;

    void saveAll();

}

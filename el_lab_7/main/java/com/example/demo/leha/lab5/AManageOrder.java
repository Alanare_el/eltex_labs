package com.example.demo.leha.lab5;

import com.example.demo.leha.lab2.Order;
import com.example.demo.leha.lab7.Exception.OrderDelException;

import java.util.PriorityQueue;

public class AManageOrder implements IOrder {

    private PriorityQueue<Order> listSaveOrder;

    AManageOrder(PriorityQueue<Order> list) {

        this.listSaveOrder = list;

    }

    @Override
    public Order readById(String id) {
        return null;
    }

    @Override
    public void saveById(String id) {

    }

    @Override
    public PriorityQueue readAll() throws OrderDelException {
        return null;
    }

    @Override
    public void saveAll() {

    }

    public String addToCard(String id) throws OrderDelException {
        return null;
    }

    public String delById(String id) throws OrderDelException {
        return null;
    }

    public PriorityQueue<Order> getListSaveOrder() {
        return listSaveOrder;
    }

    public void setListSaveOrder(PriorityQueue<Order> listSaveOrder) {
        this.listSaveOrder = listSaveOrder;
    }
}

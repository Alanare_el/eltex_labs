package com.example.demo.leha.lab5;

import com.example.demo.leha.lab2.Order;

import java.io.*;
import java.util.PriorityQueue;

public class ManagerOrderFile extends AManageOrder {

    private final String fileName = "test.dat";

    public ManagerOrderFile(PriorityQueue<Order> list) {
        super(list);
    }

    public void saveAll() {
        PriorityQueue<Order> listOrder = getListSaveOrder();
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileName))) {

            for (Order ord : listOrder) {
                objectOutputStream.writeObject(ord);
            }
            objectOutputStream.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveById(String id) {
        PriorityQueue<Order> listOrder = getListSaveOrder();
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileName))) {

            for (Order ord : listOrder) {
                if (ord.getId() == id) {
                    objectOutputStream.writeObject(ord);
                }
            }
            objectOutputStream.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public PriorityQueue readAll() {
        PriorityQueue<Order> listOrder = new PriorityQueue<>();
        System.out.println(read(listOrder));
        return null;
    }

    public Order readById(String id) {
        PriorityQueue<Order> listOrder = new PriorityQueue<>();
        listOrder = read(listOrder);
        for (Order ord : listOrder) {
            if (ord.getId() == id) {
                return ord;
            }
        }
        return null;
    }


    public PriorityQueue<Order> read(PriorityQueue<Order> listOrder) {

        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileName))) {

            while (true) {
                try {
                    listOrder.add((Order) objectInputStream.readObject());
                } catch (EOFException e) {
                    e.printStackTrace();
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return listOrder;
    }
}

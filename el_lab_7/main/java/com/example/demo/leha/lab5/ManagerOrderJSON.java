package com.example.demo.leha.lab5;

import com.example.demo.leha.lab1.Device;
import com.example.demo.leha.lab1.TV;
import com.example.demo.leha.lab2.Order;
import com.example.demo.leha.lab7.Exception.OrderDelException;
import com.google.gson.*;

import java.io.*;
import java.util.PriorityQueue;

public class ManagerOrderJSON extends AManageOrder {

    private final String fileName = "myFile.json";

    public ManagerOrderJSON(PriorityQueue<Order> list) {

        super(list);

    }

    public void saveById(String id) {

        PriorityQueue<Order> listSaveOrder = getListSaveOrder();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();
        String json = null;

        for (Order ord : listSaveOrder) {
            if (id == ord.getId()) {
                json = gson.toJson(ord);
            }
        }

        try (FileWriter writer = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            bufferedWriter.write(json);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void saveAll() {

        PriorityQueue<Order> listSaveOrder = getListSaveOrder();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();
        try (FileWriter writer = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            for (Order ord : listSaveOrder) {
                String json = gson.toJson(ord);
                bufferedWriter.write(json);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public PriorityQueue<Order> readAll() throws OrderDelException {
        PriorityQueue<Order> listOrder = new PriorityQueue<>();

        if (!new File(fileName).exists()) throw new OrderDelException(2);

        try (FileReader reader = new FileReader(fileName)) {

            JsonStreamParser p = new JsonStreamParser(reader);
            Gson gson = new GsonBuilder().create();

            while (p.hasNext()) {
                JsonElement element = p.next();
                if (element.isJsonObject()) {

                    Order order = gson.fromJson(element, Order.class);
                    listOrder.add(order);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOrder;
    }

    public Order readById(String id) {

        try (FileReader reader = new FileReader(fileName)) {

            JsonStreamParser p = new JsonStreamParser(reader);
            Gson gson = new GsonBuilder().create();

            while (p.hasNext()) {
                JsonElement element = p.next();
                if (element.isJsonObject()) {
                    if (id.equals(element.getAsJsonObject().get("id").getAsString())) {
                        Order order = gson.fromJson(element, Order.class);
                        return order;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String addToCard(String card_id) throws OrderDelException{
        PriorityQueue<Order> listOrder = readAll();

        for (Order order : listOrder) {
            if (order.getShoppingCart().getId().equals(card_id)) {
                Order orderTmp = order;
                listOrder.remove(order);
                Device device = new TV();
                device.create();
                orderTmp.getShoppingCart().add(device);
                listOrder.add(orderTmp);
                setListSaveOrder(listOrder);
                saveAll();
                return device.getId();
            }
        }
        return null;
    }

    public String delById(String id) throws OrderDelException {
        PriorityQueue<Order> listOrder = readAll();
        boolean flag = false;

        for (Order order : listOrder) {
            if (order.getId().equals(id)) {
                listOrder.remove(order);
                setListSaveOrder(listOrder);
                saveAll();
                flag = true;
                return "0";
            }
        }

        throw new OrderDelException(1);

    }
}
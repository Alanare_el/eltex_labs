package com.example.demo.leha.lab6;

import com.example.demo.leha.lab1.Device;
import com.example.demo.leha.lab1.TV;
import com.example.demo.leha.lab2.Credentials;
import com.example.demo.leha.lab2.Order;
import com.example.demo.leha.lab2.ShoppingCart;

public class Helper {

    public Order getOrder(Order order){
        TV tv = new TV();
        tv.create();

        Credentials credentials = new Credentials();
        credentials.create();

        ShoppingCart<Device> shoppingCart = new ShoppingCart<>();
        shoppingCart.add(tv);

        order.setCredentials(credentials);
        order.setShoppingCart(shoppingCart);
        return order;
    }


}

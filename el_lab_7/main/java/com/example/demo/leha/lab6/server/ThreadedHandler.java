package com.example.demo.leha.lab6.server;

import com.example.demo.leha.lab2.Order;

import java.io.*;
import java.net.*;
import java.util.Map;
import java.util.PriorityQueue;

public class ThreadedHandler extends Thread {

    private Socket socket;
    private PriorityQueue<Order> priorityQueue;
    private Map<Order, Address> addressMap;
    private int port;
    private int id;

    public ThreadedHandler(Socket incomingSocket, PriorityQueue<Order> queue,
                           Map<Order, Address> addressMap, int id){

        this.socket = incomingSocket;
        this.id = id;
        this.priorityQueue = queue;
        this.addressMap = addressMap;

        System.out.println("connect");

    }

    public void run(){
        handleOrder();
    }

    private Order receiveMessage() {
        Order order = null;
        try (ObjectInputStream in = new ObjectInputStream(socket.getInputStream())) {
            port = in.readInt();
            order = (Order) in.readObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        }
        return order;
    }

    private void handleOrder() {
        Order order = receiveMessage();
        synchronized (addressMap) {
            priorityQueue.add(order);
            addressMap.put(order, new Address(socket.getInetAddress(), port));
        }

        System.out.println(order);
    }

}

package com.example.demo.leha.lab1;

public class GameConsole extends Device {

    private String CPU;
    private String RAM;

    public GameConsole() {
        super();
        this.setName("GameConsole");
        this.CPU = "AMD Jaguar";
        this.RAM = "GDDR5 8 gb";
    }

}

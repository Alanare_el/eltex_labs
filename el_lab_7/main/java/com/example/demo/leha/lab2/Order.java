package com.example.demo.leha.lab2;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class Order implements Comparable, Serializable {

    private String id;
    private boolean status;
    private long timeCreate;
    private long timeWaiting = 10 * 1000;
    private ShoppingCart shoppingCart;
    private Credentials credentials;

    public Order() {
        Date date = new Date();
        timeCreate = date.getTime();
        id = UUID.randomUUID().toString();
        status = false;
        shoppingCart = new ShoppingCart();
        credentials = new Credentials();
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", status=" + status +
                ", timeCreate=" + timeCreate +
                ", timeWaiting=" + timeWaiting +
                ", shoppingCart=" + shoppingCart +
                ", credentials=" + credentials +
                '}';
    }

    public Long getTimeCreate() {
        return timeCreate;
    }

    public long getTimeWaiting() {
        return timeWaiting;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public boolean getStatus() {
        return status;
    }

    public void setTimeCreate(long timeCreate) {
        this.timeCreate = timeCreate;
    }

    public void setTimeWaiting(long timeWaiting) {
        this.timeWaiting = timeWaiting;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}

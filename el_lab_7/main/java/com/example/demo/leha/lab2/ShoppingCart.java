package com.example.demo.leha.lab2;

import com.example.demo.leha.lab1.Device;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.UUID;

public class ShoppingCart<T extends Device>  implements Serializable {

    private String id;
    private LinkedList<T> listProd;
    private TreeSet<String> unicID;

    public ShoppingCart() {
        id = UUID.randomUUID().toString();
        listProd = new LinkedList<>();
        unicID = new TreeSet<>();
    }

    public T find(String id) {
        T dv = null;
        for (T dev : listProd) {
            if (id == dev.getId()) dv = dev;
        }
        return dv;
    }

    public void read() {
        for (T dev : listProd) {
            System.out.println(dev);
        }
    }

    public boolean add(T prod) {
        if (listProd.add(prod)) {
            unicID.add(prod.getId());
            return true;
        } else return false;
    }

    public boolean delete(T prod) {
        if (listProd.remove(prod)) return true;
        else return false;
    }

    @Override
    public String toString() {
        return "ShoppingCart{" +
                "listProd=" + listProd +
                ", unicID=" + unicID +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LinkedList<T> getListProd() {
        return listProd;
    }

    public void setListProd(LinkedList<T> listProd) {
        this.listProd = listProd;
    }

    public TreeSet<String> getUnicID() {
        return unicID;
    }

    public void setUnicID(TreeSet<String> unicID) {
        this.unicID = unicID;
    }
}

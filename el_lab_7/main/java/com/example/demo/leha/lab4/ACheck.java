package com.example.demo.leha.lab4;


import com.example.demo.leha.lab2.Order;

import java.util.PriorityQueue;

public abstract class ACheck extends Thread {
    private PriorityQueue<Order> litsOrd;

    ACheck(PriorityQueue<Order> list) {
        this.litsOrd = list;
    }

    @Override
    public void run() {
        check();
    }

    abstract void check();

    public PriorityQueue<Order> getLitsOrd() {
        return litsOrd;
    }

    public void setLitsOrd(PriorityQueue<Order> litsOrd) {
        this.litsOrd = litsOrd;
    }
}

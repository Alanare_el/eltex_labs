package com.example.demo.leha.lab4;

import com.example.demo.leha.lab2.Order;

import java.util.PriorityQueue;

public class WaitingCheck extends ACheck {

    public WaitingCheck(PriorityQueue<Order> list) {
        super(list);
    }

    @Override
    public void check() {
        PriorityQueue<Order> listOrd = getLitsOrd();
        synchronized (listOrd) {
            for (Order ord : listOrd) {
                if (!ord.getStatus()) ord.setStatus(true);
            }
        }
    }
}

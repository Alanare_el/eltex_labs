package com.example.demo.leha.lab4;


import com.example.demo.leha.lab2.Order;

import java.util.Date;
import java.util.PriorityQueue;

public class ProcessingCheck extends ACheck {

    public ProcessingCheck(PriorityQueue<Order> list) {
        super(list);
    }

    @Override
    public void check() {
        PriorityQueue<Order> listOrd = getLitsOrd();
        synchronized (listOrd) {
            Date date = new Date();
            long currentTime = date.getTime();
            synchronized (listOrd) {
                for (Order ord : listOrd) {
                    if (currentTime >= ord.getTimeCreate() + ord.getTimeWaiting() && ord.getStatus())
                        listOrd.remove(ord);
                }
            }
        }
    }
}

package com.leha.lab2;

import com.leha.lab1.Device;

import java.util.LinkedList;
import java.util.TreeSet;

public class ShoppingCart<T extends Device> {

    private LinkedList<T> listProd;
    private TreeSet<String> unicID;

    public ShoppingCart() {
        listProd = new LinkedList<>();
        unicID = new TreeSet<>();
    }

    public T find(String id) {
        T dv = null;
        for (T dev : listProd) {
            if (id == dev.getId()) dv = dev;
        }
        return dv;
    }

    public void read() {
        for (T dev : listProd) {
            System.out.println(dev);
        }
    }

    public boolean add(T prod) {
        if (listProd.add(prod)) {
            unicID.add(prod.getId());
            return true;
        } else return false;
    }

    public boolean delete(T prod) {
        if (listProd.remove(prod)) return true;
        else return false;
    }

    public LinkedList<T> getListProd() {
        return listProd;
    }

    public void setListProd(LinkedList<T> listProd) {
        this.listProd = listProd;
    }

    public TreeSet<String> getUnicID() {
        return unicID;
    }

    public void setUnicID(TreeSet<String> unicID) {
        this.unicID = unicID;
    }
}

package com.leha.lab4;

import com.leha.lab2.Order;

import java.util.Date;
import java.util.PriorityQueue;

public class ProcessingCheck extends ACheck {

    public ProcessingCheck(PriorityQueue<Order> list) {
        super(list);
    }

    @Override
    void check() {
        PriorityQueue<Order> listOrd = getLitsOrd();
        synchronized (listOrd) {
            Date date = new Date();
            long currentTime = date.getTime();
            for (Order ord : listOrd) {
                if (currentTime >= ord.getTimeCreate() + ord.getTimeWaiting() && ord.getStatus()) listOrd.remove(ord);
            }
        }
    }
}

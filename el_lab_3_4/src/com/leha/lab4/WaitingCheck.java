package com.leha.lab4;

import com.leha.lab2.Order;

import java.util.PriorityQueue;

public class WaitingCheck extends ACheck {

    public WaitingCheck(PriorityQueue<Order> list) {
        super(list);
    }

    @Override
    void check() {
        PriorityQueue<Order> listOrd = getLitsOrd();
        synchronized (listOrd) {
            for (Order ord : listOrd) {
                if (!ord.getStatus()) ord.setStatus(true);
            }
        }
    }
}

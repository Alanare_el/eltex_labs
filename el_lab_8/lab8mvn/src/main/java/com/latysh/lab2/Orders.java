package com.latysh.lab2;

import java.util.Date;
import java.util.HashMap;
import java.util.PriorityQueue;

public class Orders<T extends Order> {

    private PriorityQueue<T> listOrder;
    private HashMap<Long, T> timeOrder;

    public Orders() {
        listOrder = new PriorityQueue<>();
        timeOrder = new HashMap<>();
    }

    public void buy(T order) {
        timeOrder.put(order.getTimeCreate(), order);
        listOrder.add(order);
    }

    public void read() {
        for (T ord : listOrder) {
            System.out.println(ord);
        }
    }

    public void checkOrder() {
        Date date = new Date();
        long currentTime = date.getTime();
        for (T ord : listOrder) {
            if (currentTime >= ord.getTimeCreate() + ord.getTimeWaiting() && ord.getStatus()) listOrder.remove(ord);
        }
    }

    public PriorityQueue<T> getListOrder() {
        return listOrder;
    }

    public void setListOrder(PriorityQueue<T> listOrder) {
        this.listOrder = listOrder;
    }

    public HashMap<Long, T> getTimeOrder() {
        return timeOrder;
    }

    public void setTimeOrder(HashMap<Long, T> timeOrder) {
        this.timeOrder = timeOrder;
    }
}

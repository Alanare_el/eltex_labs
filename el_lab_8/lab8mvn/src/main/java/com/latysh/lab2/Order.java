package com.latysh.lab2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "orders")
public class Order implements Comparable, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "identificator")
    private String identificator;
    @Column(name = "status")
    private boolean status;
    @Column(name = "time_create")
    private long timeCreate;
    @Transient
    private long timeWaiting = 10 * 1000;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumn(name="shopping_cart_id")
    private ShoppingCart shoppingCart;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumn(name = "credentials_id")
    private Credentials credentials;

    public Order() {
        Date date = new Date();
        timeCreate = date.getTime();
        identificator = UUID.randomUUID().toString();
        status = false;
        shoppingCart = new ShoppingCart();
        credentials = new Credentials();
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + identificator + '\'' +
                ", status=" + status +
                ", timeCreate=" + timeCreate +
                ", timeWaiting=" + timeWaiting +
                ", shoppingCart=" + shoppingCart +
                ", credentials=" + credentials +
                '}';
    }

    public Long getTimeCreate() {
        return timeCreate;
    }

    public long getTimeWaiting() {
        return timeWaiting;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public boolean getStatus() {
        return status;
    }

    public void setTimeCreate(long timeCreate) {
        this.timeCreate = timeCreate;
    }

    public void setTimeWaiting(long timeWaiting) {
        this.timeWaiting = timeWaiting;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getIdentificator() {
        return identificator;
    }

    public void setIdentificator(String id) {
        this.identificator = id;
    }

    public boolean isStatus() {
        return status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}

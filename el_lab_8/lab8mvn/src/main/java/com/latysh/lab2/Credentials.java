package com.latysh.lab2;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Random;
import java.util.UUID;

@Entity
@Table(name = "credentials")
public class Credentials implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "identificator")
    private String identificator;
    @Column(name = "surname")
    private String surname;
    @Column(name = "name")
    private String name;
    @Column(name = "middleName")
    private String middleName;
    @Column(name = "eMail")
    private String eMail;

    public Credentials() {
        identificator = UUID.randomUUID().toString();
    }

    public void create() {
        Random rnd = new Random(System.currentTimeMillis());

        String[] dictionaryName = {"Aleks", "Vova", "Ivan", "Vasya"};
        String[] dictionarySurname = {"Alekseev", "Petrov", "Ivanov", "Sidorov"};
        String[] dictionaryMiddleName = {"Alekseevich", "Viktorovich", "Ivanovich", "Popovich"};
        String[] dictionaryEmail = {"one_love@love.ru", "sidr1337@sidr.ru", "zhuk228@mail.ru", "trofimloh@loh.ru"};

        this.name = dictionaryName[0 + rnd.nextInt(3 - 0 + 1)];
        this.surname = dictionarySurname[0 + rnd.nextInt(3 - 0 + 1)];
        this.middleName = dictionaryMiddleName[0 + rnd.nextInt(3 - 0 + 1)];
        this.eMail = dictionaryEmail[0 + rnd.nextInt(3 - 0 + 1)];
    }

    @Override
    public String toString() {
        return "Credentials{" +
                "idedentificator='" + identificator + '\'' +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", middleName='" + middleName + '\'' +
                ", eMail='" + eMail + '\'' +
                '}';
    }

    public String getIdentificator() {
        return identificator;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String geteMail() {
        return eMail;
    }

    public void setIdentificator(String id) {
        this.identificator = id;
    }

    public void setSurname(String surname) {
        surname = surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public int getId() {
        return id;
    }

    public void setId(int credentials_id) {
        this.id = credentials_id;
    }

}

package com.latysh.lab2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.latysh.lab1.Device;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "shopping_carts")
public class ShoppingCart<T extends Device> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "identificator")
    private String identificator;
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToMany (fetch = FetchType.LAZY,targetEntity = Device.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "shopping_cart_id")
    private Set<T> listProd;
    @Transient
    private TreeSet<String> unicID;

    public ShoppingCart() {
        identificator = UUID.randomUUID().toString();
        listProd = new HashSet<>();
        unicID = new TreeSet<>();
    }

    public T find(String id) {
        T dv = null;
        for (T dev : listProd) {
            if (id == dev.getIdentificator()) dv = dev;
        }
        return dv;
    }

    public void read() {
        for (T dev : listProd) {
            System.out.println(dev);
        }
    }

    public boolean add(T prod) {
        if (listProd.add(prod)) {
            unicID.add(prod.getIdentificator());
            return true;
        } else return false;
    }

    public boolean delete(T prod) {
        if (listProd.remove(prod)) return true;
        else return false;
    }

    @Override
    public String toString() {
        return "ShoppingCart{" +
                "id='" + identificator + '\'' +
                ", listProd=" + listProd +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentificator() {
        return identificator;
    }

    public void setIdentificator(String identificator) {
        this.identificator = identificator;
    }

    public Set<T> getListProd() {
        return listProd;
    }

    public void setListProd(Set<T> listProd) {
        this.listProd = listProd;
    }

    public TreeSet<String> getUnicID() {
        return unicID;
    }

    public void setUnicID(TreeSet<String> unicID) {
        this.unicID = unicID;
    }
}

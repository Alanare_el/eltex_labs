package com.latysh.lab7.controllers;//package com.leha.com.latysh.lab7.controllers;
//
//
//import com.leha.com.latysh.lab7.Exception.OrderDelException;
//import com.leha.com.latysh.lab7.service.OrderService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//import org.apache.log4j.Logger;
//
//@RestController
//public class OrderController {
//
//    final static Logger logger = Logger.getLogger(OrderController.class);
//
//    @Autowired
//    private OrderService orderService;
//
//    @GetMapping("/")
//    public @ResponseBody
//    String getAllOrders(@RequestParam(value = "command", required = false) String first_param,
//                        @RequestParam(value = "order_id", required = false) String orderId,
//                        @RequestParam(value = "card_id", required = false) String cardId) {
//
//        try {
//
//            if (first_param.equals("readAll")) return orderService.getAllOrders();
//            if (first_param.equals("readById") && orderId != null) return orderService.getById(orderId);
//            if (first_param.equals("addToCard") && cardId != null) return orderService.addToCard(cardId);
//            if (first_param.equals("delById") && orderId != null) return orderService.delById(orderId);
//
//            throw new OrderDelException(3);
//
//        } catch (OrderDelException e) {
//            logger.warn("Sorry, something went wrong!", e);
//            return String.valueOf(e.getCode());
//        }
//
//    }
//
//}

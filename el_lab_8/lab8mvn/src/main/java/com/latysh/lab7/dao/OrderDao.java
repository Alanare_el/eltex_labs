package com.latysh.lab7.dao;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.latysh.lab2.Order;
import com.latysh.lab5.AManageOrder;
import com.latysh.lab5.ManagerOrderJSON;
import com.latysh.lab7.Exception.OrderDelException;
import org.springframework.stereotype.Repository;

import java.util.PriorityQueue;

@Repository
public class OrderDao {

    private Gson gson = new GsonBuilder().create();
    private PriorityQueue<Order> orders = new PriorityQueue<>();

    private AManageOrder managerOrder = new ManagerOrderJSON(orders);

    public OrderDao() throws OrderDelException {
    }

    public String getAllOrders() throws OrderDelException {
        return gson.toJson(managerOrder.readAll());
    }

    public String getById(String id) {
        return gson.toJson(managerOrder.readById(id));
    }

    public String addToCard(String id) throws OrderDelException{
        return managerOrder.addToCard(id);
    }

    public String delById(String id) throws OrderDelException {
        return managerOrder.delById(id);
    }
}

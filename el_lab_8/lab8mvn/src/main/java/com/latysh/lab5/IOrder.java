package com.latysh.lab5;

import com.latysh.lab2.Order;
import com.latysh.lab7.Exception.OrderDelException;

import java.util.PriorityQueue;

public interface IOrder {

    Order readById(String id);

    void saveById(String id);

    PriorityQueue readAll() throws OrderDelException;

    void saveAll();

}

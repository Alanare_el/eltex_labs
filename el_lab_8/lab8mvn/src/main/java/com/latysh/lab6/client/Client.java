package com.latysh.lab6.client;

import com.latysh.lab2.Order;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.Date;

public class Client {

    private int portUDP;
    private int portTCP;
    private InetAddress address;
    private long timeSend;
    private long timeProcessed;

    public Client(int port) {
        this.portUDP = port;
        this.portTCP = -1;
        this.timeSend = 0;
        this.timeProcessed = 0;
    }

    public static void main(String[] args) {
        System.out.println("Receiver is running");
        new Client(Integer.parseInt(args[0])).clientGo();
    }

    private void clientGo() {
        receivePort();

        while (true) {

            sendOrder();
            waitFlag();

            System.out.println(timeProcessed - timeSend);
        }
    }

    private void receivePort() {

        try (DatagramSocket ds = new DatagramSocket(portUDP)) {

            while (true) {
                DatagramPacket pack = new DatagramPacket(new byte[1024], 1024);
                ds.receive(pack);
                portTCP = Integer.parseInt(new String(pack.getData(), 0, pack.getLength()));
                System.out.println(portTCP);
                address = pack.getAddress();
                System.out.println(address);
                if (address.equals(address))
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendOrder() {

        try (Socket socket = new Socket(address, portTCP);
             ObjectOutputStream serializer = new ObjectOutputStream(socket.getOutputStream());
             DatagramSocket ds = new DatagramSocket(portUDP)) {

            Order c = new Order();

            //new Helper(c);

            System.out.println(c);

            timeSend = c.getTimeCreate();

            serializer.writeInt(portUDP);
            serializer.writeObject(c);
            serializer.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void waitFlag() {
        while (true) {
            try (DatagramSocket ds = new DatagramSocket(portUDP)) {
                byte[] buff = new byte[50];
                DatagramPacket datagramPacket = new DatagramPacket(buff, buff.length);
                ds.receive(datagramPacket);

                Date date = new Date();
                timeProcessed = date.getTime();
                InetAddress address = datagramPacket.getAddress();
                if (address.equals(address))
                    break;
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


    }
}
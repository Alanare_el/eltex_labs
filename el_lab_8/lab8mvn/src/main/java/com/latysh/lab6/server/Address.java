package com.latysh.lab6.server;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.List;

public class Address {
    InetAddress address;
    int port;

    Address(InetAddress address, int port) {
        this.address = address;
        this.port = port;
    }

    public static InetAddress getBroadcastAddress(InetAddress address) {
        try {
            InetAddress broadcast = null;
            NetworkInterface networkInterface = NetworkInterface.getByInetAddress(address);
            if (networkInterface == null)
                return null;
            List<InterfaceAddress> list = networkInterface.getInterfaceAddresses();

            for (InterfaceAddress interfaceAddress : list) {
                if (interfaceAddress.getBroadcast() != null)
                    broadcast = interfaceAddress.getBroadcast();
            }
            return broadcast;
        } catch (SocketException e) {
            e.printStackTrace();
            return null;
        }
    }
}
package com.latysh.lab6;

import com.latysh.lab1.Device;
import com.latysh.lab1.TV;
import com.latysh.lab2.Credentials;
import com.latysh.lab2.Order;
import com.latysh.lab2.ShoppingCart;

public class Helper {

    public Order getOrder(Order order){
        TV tv = new TV();
        tv.create();

        Credentials credentials = new Credentials();
        credentials.create();

        ShoppingCart<Device> shoppingCart = new ShoppingCart<>();
        shoppingCart.add(tv);

        order.setCredentials(credentials);
        order.setShoppingCart(shoppingCart);
        return order;
    }


}

package com.latysh.lab8.service;

import com.latysh.lab8.dao.HibernateOrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HibernateService {

    @Autowired
    HibernateOrderDao hibernateOrderDao;

    public Object getAllOrders() {
        return hibernateOrderDao.getAll();
    }

    public Object getById(String id){
        return hibernateOrderDao.getById(id);
    }

    public String addToCart(String id){
        return hibernateOrderDao.addToCart(id);
    }

    public String delById(String id){ return hibernateOrderDao.delById(id);}
}

package com.latysh.lab8.dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.latysh.lab1.SetTop;
import com.latysh.lab2.Order;
import com.latysh.lab6.Helper;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class HibernateOrderDao {

    @PersistenceContext
    private EntityManager entityManager;

    private Gson gson = new GsonBuilder().create();

    public Object getAll() {

        List<Order> orders = entityManager.createQuery("FROM Order", Order.class).getResultList();
        return orders;

    }

    public Object getById(String id){
        List<Order> orders = entityManager.createQuery("FROM Order", Order.class).getResultList();
        for(Order order : orders){
            if(order.getIdentificator().equals(id)){
                return order;
            }
        }
        return null;
    }

    public String addToCart(String id){

        int shoppingCartId = entityManager
                .createQuery("SELECT id FROM ShoppingCart WHERE identificator = :identificator", Integer.class)
                .setParameter("identificator", id)
                .getSingleResult();
        Order order = entityManager
                .createQuery("FROM Order WHERE shopping_cart_id = :shoppingCartId", Order.class)
                .setParameter("shoppingCartId",shoppingCartId)
                .getSingleResult();
        SetTop setTop = new SetTop();
        setTop.create();
        order.getShoppingCart().add(setTop);
        entityManager.persist(order);
        return setTop.getIdentificator();

    }

    public String delById(String id){

        Order order = entityManager
                .createQuery("FROM Order WHERE identificator = :identificator",Order.class)
                .setParameter("identificator", id)
                .getSingleResult();

            entityManager.remove(order);

        return "0";
    }


}

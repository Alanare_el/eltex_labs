package com.latysh.lab8.controllers;

import com.latysh.lab8.service.HibernateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HibernateOrderController {

    @Autowired
    HibernateService hibernateService;

    @GetMapping("/")
    public Object getAllOrders(@RequestParam(value = "command", required = false) String first_param,
                        @RequestParam(value = "order_id", required = false) String orderId,
                        @RequestParam(value = "card_id", required = false) String cardId) {

        if (first_param.equals("readAll")) return hibernateService.getAllOrders();
        if (first_param.equals("readById") && orderId != null) return hibernateService.getById(orderId);
        if (first_param.equals("addToCard") && cardId != null) return hibernateService.addToCart(cardId);
        if (first_param.equals("delById") && orderId != null) return hibernateService.delById(orderId);

        return null;
    }
}

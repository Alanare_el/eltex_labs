package com.latysh.lab1;

public interface ICrudAction {

    void create();

    void read();

    void update();

    void delete();

}

package com.latysh.lab1;

import javax.persistence.Entity;

@Entity
public class GameConsole extends Device {

    private String CPU;
    private String RAM;

    public GameConsole() {
        super();
        this.setName("GameConsole");
        this.CPU = "AMD Jaguar";
        this.RAM = "GDDR5 8 gb";
    }

    public String getCPU() {
        return CPU;
    }

    public void setCPU(String CPU) {
        this.CPU = CPU;
    }

    public String getRAM() {
        return RAM;
    }

    public void setRAM(String RAM) {
        this.RAM = RAM;
    }
}

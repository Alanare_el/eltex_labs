package com.latysh.lab1;

import javax.persistence.Entity;

@Entity
public class SetTop extends Device {

    private String HDTV;

    public SetTop() {
        super();
        this.setName("SetTop");
        this.HDTV = "1080p";
    }

    public String getHDTV() {
        return HDTV;
    }

    public void setHDTV(String HDTV) {
        this.HDTV = HDTV;
    }
}

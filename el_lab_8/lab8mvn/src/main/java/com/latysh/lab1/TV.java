package com.latysh.lab1;

import javax.persistence.Entity;

@Entity
public class TV extends Device {

    private String diagonal;

    public TV() {
        super();
        this.setName("TV");
        this.diagonal = "2.2 m";
    }

    public String getDiagonal() {
        return diagonal;
    }

    public void setDiagonal(String diagonal) {
        this.diagonal = diagonal;
    }
}

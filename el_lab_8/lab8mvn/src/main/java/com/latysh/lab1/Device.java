package com.latysh.lab1;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Device implements ICrudAction, Serializable {

    @Transient
    private static int count;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name="identificator")
    private String identificator;
    @Column(name="name")
    private String name;
    @Column(name = "price")
    private String price;
    @Column(name="firm")
    private String firm;
    @Column(name="model")
    private String model;


    public Device() {
        identificator = UUID.randomUUID().toString();
        count++;
    }


    @Override
    public void create() {
        Random rnd = new Random(System.currentTimeMillis());


        String[] dictionaryName = {"ponch", "vonch", "kolob", "kildeboer"};
        String[] dictionaryPrice = {"1337", "10300", "7900", "29900"};
        String[] dictionaryFirm = {"OAO ponch", "OAO ponch", "ЗАО kolob", "OAO kildeboer"};
        String[] dictionaryModel = {"ponch2003", "vonch97", "kolob18", "kildeboer_one"};

        this.name = dictionaryName[0 + rnd.nextInt(3 - 0 + 1)];
        this.price = dictionaryPrice[0 + rnd.nextInt(3 - 0 + 1)];
        this.firm = dictionaryFirm[0 + rnd.nextInt(3 - 0 + 1)];
        this.model = dictionaryModel[0 + rnd.nextInt(3 - 0 + 1)];

        count++;
    }

    @Override
    public void read() {
        System.out.println(name);
        System.out.println(price);
        System.out.println(firm);
        System.out.println(model);
    }

    @Override
    public void update() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите наименивание:");
        this.name = in.nextLine();
        System.out.println("Введите цену:");
        this.price = in.nextLine();
        System.out.println("Введите наименование фирмы:");
        this.firm = in.nextLine();
        System.out.println("Введите наименование модели:");
        this.model = in.nextLine();
    }

    @Override
    public void delete() {
        this.identificator = null;
        this.name = null;
        this.price = null;
        this.model = null;
        this.firm = null;
        count--;
    }

    @Override
    public String toString() {
        return "Device{" +
                "identificator='" + identificator + '\'' +
                ", name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", firm='" + firm + '\'' +
                ", model='" + model + '\'' +
                '}';
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Device.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentificator() {
        return identificator;
    }

    public void setIdentificator(String identificator) {
        this.identificator = identificator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}

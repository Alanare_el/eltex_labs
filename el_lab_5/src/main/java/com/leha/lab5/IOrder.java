package com.leha.lab5;

public interface IOrder {

    void readById(String id);
    void saveById(String id);
    void readAll();
    void saveAll();

}

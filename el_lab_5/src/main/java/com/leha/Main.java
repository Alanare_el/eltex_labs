package com.leha;

import com.leha.lab1.Device;
import com.leha.lab1.GameConsole;
import com.leha.lab1.SetTop;
import com.leha.lab1.TV;
import com.leha.lab2.Credentials;
import com.leha.lab2.Order;
import com.leha.lab2.Orders;
import com.leha.lab2.ShoppingCart;
import com.leha.lab4.ProcessingCheck;
import com.leha.lab4.WaitingCheck;
import com.leha.lab5.ManagerOrderFile;
import com.leha.lab5.ManagerOrderJSON;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        int kol = Integer.parseInt(args[1]);
        int opr;
        String vid = args[0];
        ShoppingCart shoppingCart = new ShoppingCart();
        Credentials credentials = new Credentials();
        Orders orders = new Orders();
        Order order;

        Scanner in = new Scanner(System.in);

        boolean cheker = true;

        while (cheker) {
            System.out.println("0 - Оформление покупки;" + "\n"
                    + "1 - Проверка заказов;" + "\n"
                    + "2 - Просмотр списка заказов;" + "\n"
                    + "3 - Просмотр корзины;" + "\n"
                    + "4 - Проверка заказов на обработку;" + "\n"
                    + "5 - Проверка заказов на ожидание;" + "\n"
                    + "6 - Выход;");
            System.out.println("Ведите номер опрерации:" + "\n");
            opr = in.nextInt();
            switch (opr) {
                case 0:
                    for (int i = 0; i < kol; i++) {
                        credentials.create();
                        order = new Order();
                        if (vid.equals("TV")) {
                            TV objTV = new TV();
                            objTV.create();
                            objTV.update();
//                objTV.read();
//                objTV.delete();
                            shoppingCart.add(objTV);
                            Device obj = new TV();
                            obj = shoppingCart.find(objTV.getId());
                            System.out.println(obj);
                        }

                        if (vid.equals("GameConsole")) {
                            GameConsole objGC = new GameConsole();
                            objGC.create();
                            objGC.update();
//                objGC.read();
//                objGC.delete();
                            shoppingCart.add(objGC);
                        }

                        if (vid.equals("SetTop")) {
                            SetTop objST = new SetTop();
                            objST.create();
                            objST.update();
//                objST.read();
//                objST.delete();
                            shoppingCart.add(objST);

                        }
                        order.setCredentials(credentials);
                        order.setShoppingCart(shoppingCart);
                        orders.buy(order);
                    }
                    break;
                case 1:
                    orders.checkOrder();
                    break;
                case 2:
                    orders.read();
                    break;
                case 3:
                    shoppingCart.read();
                    break;
                case 4:
                    ProcessingCheck processingCheck = new ProcessingCheck(orders.getListOrder());
                    processingCheck.start();
                    processingCheck.join();
                    break;
                case 5:
                    WaitingCheck waitingCheck = new WaitingCheck(orders.getListOrder());
                    waitingCheck.start();
                    waitingCheck.join();
                    break;
                case 6:
                    ManagerOrderJSON managerOrderJSON=new ManagerOrderJSON(orders.getListOrder());
                    Order order1= (Order) orders.getListOrder().peek();
                    managerOrderJSON.saveById(order1.getId());
                    break;
                case 7:
                    managerOrderJSON = new ManagerOrderJSON(orders.getListOrder());
                    managerOrderJSON.saveAll();
                    break;
                case 8:
                    managerOrderJSON = new ManagerOrderJSON(orders.getListOrder());
                    managerOrderJSON.readAll();
                    break;
                case 9:
                    managerOrderJSON=new ManagerOrderJSON(orders.getListOrder());
                    Order order2 = (Order) orders.getListOrder().peek();
                    managerOrderJSON.readById(order2.getId());
                    break;
                case 10:
                    ManagerOrderFile managerOrderFile = new ManagerOrderFile(orders.getListOrder());
                    managerOrderFile.saveAll();
                    break;
                case 11:
                    managerOrderFile = new ManagerOrderFile(orders.getListOrder());
                    managerOrderFile.readAll();
                case 12:
                    managerOrderFile = new ManagerOrderFile(orders.getListOrder());
                    order2=(Order) orders.getListOrder().peek();
                    managerOrderFile.saveById(order2.getId());
                case 13:
                    managerOrderFile = new ManagerOrderFile(orders.getListOrder());
                    order2=(Order) orders.getListOrder().peek();
                    managerOrderFile.readById(order2.getId());
                case 14:
                    cheker = false;
                    break;

            }
        }
    }
}

package com.leha.lab5;

import com.google.gson.*;
import com.leha.lab2.Order;

import java.io.*;
import java.util.PriorityQueue;

public class ManagerOrderJSON extends AManageOrder {

    private final String fileName = "test.json";

    public ManagerOrderJSON(PriorityQueue<Order> list) {
        super(list);
    }

    public void saveById(String id) {

        PriorityQueue<Order> listSaveOrder = getListSaveOrder();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();
        String json = null;

        for (Order ord : listSaveOrder) {
            if (id == ord.getId()) {
                json = gson.toJson(ord);
            }
        }

        try(FileWriter writer = new FileWriter(fileName, true);
            BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            bufferedWriter.write(json);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void saveAll() {

        PriorityQueue<Order> listSaveOrder = getListSaveOrder();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();
        try(FileWriter writer = new FileWriter(fileName, true);
            BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            for (Order ord : listSaveOrder) {
                String json = gson.toJson(ord);
                bufferedWriter.write(json);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void readAll() {
        PriorityQueue<Order> listOrder = new PriorityQueue<>();

        try(FileReader reader = new FileReader(fileName)) {

            JsonStreamParser p = new JsonStreamParser(reader);
            Gson gson = new GsonBuilder().create();

            while (p.hasNext()) {
                JsonElement element = p.next();
                if (element.isJsonObject()) {

                    Order order = gson.fromJson(element, Order.class);
                    listOrder.add(order);
                }
            }
            System.out.println(listOrder);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void readById(String id) {

        try(FileReader reader = new FileReader(fileName)) {

            JsonStreamParser p = new JsonStreamParser(reader);
            Gson gson = new GsonBuilder().create();

            while (p.hasNext()) {
                JsonElement element = p.next();
                if (element.isJsonObject()) {
                    if (id.equals(element.getAsJsonObject().get("id").getAsString())) {
                        Order order = gson.fromJson(element, Order.class);
                        System.out.println(order);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
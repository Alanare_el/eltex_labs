package com.leha.lab5;

import com.leha.lab2.Order;

import java.io.FileNotFoundException;
import java.util.PriorityQueue;

public class AManageOrder implements IOrder {

    private PriorityQueue<Order> listSaveOrder;

    AManageOrder(PriorityQueue<Order> list){

        this.listSaveOrder=list;

    }

    @Override
    public void readById(String id) {

    }

    @Override
    public void saveById(String id) {

    }

    @Override
    public void readAll(){

    }

    @Override
    public void saveAll() {

    }

    public PriorityQueue<Order> getListSaveOrder() {
        return listSaveOrder;
    }

    public void setListSaveOrder(PriorityQueue<Order> listSaveOrder) {
        this.listSaveOrder = listSaveOrder;
    }
}

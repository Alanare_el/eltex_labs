package com.leha.lab1;

import com.leha.lab1.Device;

public class TV extends Device {

    private String diagonal;

    public TV() {
        super();
        this.setName("TV");
        this.diagonal = "2.2 m";
    }

}

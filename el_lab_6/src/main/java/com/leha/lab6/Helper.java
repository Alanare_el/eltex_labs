package com.leha.lab6;

import com.leha.lab1.Device;
import com.leha.lab1.TV;
import com.leha.lab2.Credentials;
import com.leha.lab2.Order;
import com.leha.lab2.ShoppingCart;

public class Helper {

    public Helper(Order order){
        TV tv = new TV();
        tv.create();

        Credentials credentials = new Credentials();
        credentials.create();

        ShoppingCart<Device> shoppingCart = new ShoppingCart<>();
        shoppingCart.add(tv);

        order.setCredentials(credentials);
        order.setShoppingCart(shoppingCart);
    }
}

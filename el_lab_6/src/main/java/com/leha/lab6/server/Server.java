package com.leha.lab6.server;

import com.leha.lab2.Order;
import com.leha.lab4.ProcessingCheck;
import com.leha.lab4.WaitingCheck;

import java.io.IOException;
import java.net.*;
import java.util.*;

public class Server {

    private static int portTCP = 8080;
    private InetAddress localAddress;
    private Map<Order, Address> addressMap;
    private static PriorityQueue<Order> queueOrder;
    private List<Integer> udpPorts;




    public Server(InetAddress address, int portTCP, List<Integer> udpPorts) {
        this.portTCP = portTCP;
        this.udpPorts = udpPorts;
        this.localAddress = address;

        queueOrder = new PriorityQueue<>();
        addressMap = new HashMap<>();
    }

    public static void main(String[] args) throws UnknownHostException, InterruptedException {

        List<Integer> udpPorts = new LinkedList<>();
        InetAddress local = InetAddress.getByName(args[0]);
        for (int i = 1; i < args.length; i++)
            udpPorts.add(Integer.valueOf(args[i]));

        new Server(local, portTCP, udpPorts).serverGo();

    }

    private void serverGo() throws InterruptedException {

        System.out.println("Server is running");

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                acceptClient();
            }
        });
        thread.start();

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                //broadcast
                InetAddress broadcast = Address.getBroadcastAddress(localAddress);
                if (broadcast == null)
                    System.out.println("Broadcast is null");

                for (Integer udpPort : udpPorts) {
                    sendMessage(portTCP, new Address(broadcast, udpPort));
                   // System.out.println(broadcast + " port:" + udpPort);
                }

                sendNotification();
            }

            private void sendNotification() {
                synchronized (queueOrder) {
                    for (Order order : queueOrder) {
                        if (order.getStatus() == true) {
                            Address address = addressMap.get(order);
                            sendMessage(1337, address);
                        }
                    }
                }
            }
        }, 0, 1700);

        thread.join();

    }

    private void sendMessage(int message, Address address) {
        try (DatagramSocket datagramSocket = new DatagramSocket()) {
            byte[] buf = String.valueOf(message).getBytes();
            DatagramPacket datagramPacket = new DatagramPacket(buf, buf.length, address.address,
                    address.port);
            try {
                datagramSocket.send(datagramPacket);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    private void acceptClient(){

        try (ServerSocket serverSocket = new ServerSocket(portTCP, 0, localAddress)) {
            while (true) {
                Socket socket = serverSocket.accept();

                int id = 0;

                Random rand = new Random();
                id = rand.nextInt(150) + 1000;

                ThreadedHandler clientThread = new ThreadedHandler(socket, queueOrder,
                        addressMap, id);
                clientThread.start();
                System.out.println("Spawning " + id);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
